/*
    Copyright (C) by Rami Ojala

    You are not allowed to change or share
    this file.
*/

#include "Utilities.h"

[[deprecated("Use std::runtime_error from <stdexcept> instead, it should be portable")]]
void ServerUtilites::ThrowError(std::string errorMessage)
{
#ifdef __unix__
    throw std::runtime_error(errorMessage);
#elif defined _WIN32
    throw std::exception(errorMessage);
#endif // OS CHECK
}
