/*
    Copyright (C) by Rami Ojala

    You are not allowed to change or share
    this file.
*/

#include <iostream>
#include <signal.h>
#include <csignal>

#include "Protocol.h"
#include "Server.h"
#include "ChatChannel.h"

volatile std::sig_atomic_t IsRunning = 0;

void SignalHandler(int SigID) // does not get called in vscode debugging, works fine when built
{
    IsRunning = 1;
}

// recv and broadcast here. reason server does not recv for you,
// is that you can more easily change protocols used to read and send
void HandleMessage(Server *server, int Socket) ///< callback function for server
{

    // USE YOUR PROTOCOL HERE
    std::map<int, Connection*> connections = server->GetConnections();

    recv(Socket, connections[Socket]->ReceiveBuffer, MESSAGEBUFFERSIZE, 0);

    if(strlen(connections[Socket]->ReceiveBuffer) > 2) // receive bufferiin tulee joka entterilla \n\r
    {

        sprintf (connections[Socket]->SendBuffer, "%u says: %s", Socket, connections[Socket]->ReceiveBuffer);

        // broadcast message to other clients but not yourself
        for (auto&& [MapSocket,SocketEvents] : connections)
        {
            if(MapSocket != Socket)
                send(MapSocket, &connections[Socket]->SendBuffer, MESSAGEBUFFERSIZE, 0);
        }
    }
}

// Handle accepting connections here, if you wish to use additional
// steps to secure the connection.
int HandleConnection(Server *server, int Socket) ///< callback function for server
{

    return accept(Socket, nullptr, nullptr);
}

int main(int argc, char *argv[])
{

    std::string IP, Port;
    if (argc < 3)
    {
        std::cout << "Defaulting IP to \"localhost\" and Port to \"60000\"" << '\n';
        IP = "localhost";
        Port = "60000";
    }
    else
    {
        IP = argv[1];
        Port = argv[2];
    }

    struct sigaction sa;
    sa.sa_handler = SignalHandler;
    sigaction(SIGINT,&sa,NULL);

    // ChatChannels *channels = new ChatChannels();
    Server *server = new Server(IP, Port, HandleMessage, HandleConnection); 

    // loop
    while (IsRunning == 0)
    {
        server->HandleEpollEvents();
    }
    
    // shutdown
    server->ShutdownServer();
    return 0;
}
