/*
    Copyright (C) by Rami Ojala

    You are not allowed to change or share
    this file.
*/

// Tasks:
// Change strings and char arrays to unsigned chars

#include "Protocol.h"

// Gets all the data in the socket. Use this one for packets with header. Return 0 when socket is closed or number of bytes received.
int VPProtocol::ReadAll(int Socket, std::string & OutputData)
{
    return ReadAll(Socket, OutputData, ReadHeader(Socket));
}

// Gets all the data in the socket. Use this one for packets without header. Return 0 when socket is closed or number of bytes sent.
int VPProtocol::ReadAll(int Socket, std::string & OutputData, int DataLength)
{
    int total = 0; // how many bytes we've received
    int bytesReceived = 0;

    char *message = new char[DataLength+1];

    memset(message, 0, DataLength + 1);

    while (total < DataLength)
    {
        // recv returns error or the number of bytes received.
        bytesReceived = recv(Socket, message + total, DataLength - total, 0);
        if (bytesReceived == 0)
        {
            return 0;
        }
        else if (bytesReceived == -1)
        {
            ServerUtilites::ThrowError("Error while receiving data from socket");
        }
        total += bytesReceived;
    }

    OutputData = message;
    delete[] message;

    return total;
}

// Generates a header for the input data and sends it all trough a socket. Return 0 when socket is closed or number of bytes sent.
int VPProtocol::SendAll(int Socket, std::string & InputData)
{

    // writeheader
    char header[HEADERBYTES] = "";
    WriteHeader(InputData.length(), header);

    int stringLength = HEADERBYTES + InputData.length()+1;

    // write package. NOTE: Cant use strcpy and strcat since they terminate at null and we need nulls in our header!
    char *message = new char[stringLength]; // plus one to prevent overflow

    memset(message, 0, stringLength + 1);

    for (int i = 0; i < HEADERBYTES; i++)
    {
        message[i] = header[i];
    }

    for (int i = HEADERBYTES; i < stringLength; i++)
    {
        if (i < stringLength)
        {
            message[i] = InputData[i - HEADERBYTES];
        }
        else
        {
            message[i] = 0;
        }
    }

    return SendAll(Socket, message, HEADERBYTES + InputData.length());
}

// Sends all data trough a socket, doesn't generate header. Return 0 when socket is closed or number of bytes sent.
int VPProtocol::SendAll(int Socket, const char * InputData, int DataLength)
{
    int total = 0;
    int bytesSent = 0;

    while (total < DataLength)
    {
        bytesSent = send(Socket, (InputData + total), (DataLength - total), 0);
        if (bytesSent == SOCKET_ERROR)
        {
            ServerUtilites::ThrowError("Error while sending data to socket");
        }

        total += bytesSent;
    }

    return total;
}

// Bit shift four long char array back to integer and return it.
unsigned int VPProtocol::ReadHeader(int Socket)
{
    std::string header = "";
    ReadAll(Socket, header, HEADERBYTES);

    char InputArray[HEADERBYTES] = "";
    const int headerLength = header.length();
    for (int i = 0; i < headerLength; i++)
    {
        InputArray[i] = header[i];
    }
    for (int i = headerLength; i < HEADERBYTES; i++)
    {
        InputArray[i] = '\0';
    }

    if (sizeof(InputArray) != HEADERBYTES)
    {
        ServerUtilites::ThrowError("Messages header length is different from accepted server header length.");
    }
    else
    {
        unsigned int messageLength = 0;
        int bitshift = 0;
        for (int i = 1; i <= HEADERBYTES; i++)
        {
            bitshift = (HEADERBYTES*BITSINBYTE - (BITSINBYTE*i));
            if (bitshift != 0)
            {
                messageLength |= (unsigned char)InputArray[HEADERBYTES - i] << bitshift;
            }
            else
            {
                messageLength |= (unsigned char)InputArray[HEADERBYTES - i];
            }

        }

        return messageLength;
    }

    return -1;
}

// Bit shift integer to four long char array and return it.
void VPProtocol::WriteHeader(int NumberToEncode, char ReturnArray[])
{
    if (ReturnArray != nullptr)
    {
        for (int i = 1; i <= HEADERBYTES; i++)
        {
            ReturnArray[HEADERBYTES - i] = (unsigned char)((unsigned int)NumberToEncode >> (HEADERBYTES*BITSINBYTE - (BITSINBYTE*i)));
        }
    }
    else
        ServerUtilites::ThrowError("Null pointer to output array while writing header");
}
