/*
    Copyright (C) by Rami Ojala

    You are not allowed to change or share
    this file.
*/

#ifndef SERVERUTILITIES_H
#define SERVERUTILITIES_H

#include <string>
#include <stdexcept>

/**

Utilities.h holds in Utility funktions that are handy
to have in our ChatServer or ChatClient

*/
class ServerUtilites
{

public:
    static void ThrowError(std::string errorMessage); /// < Used to throw correct error depending on OS

};

#endif
