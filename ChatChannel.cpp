/*
    Copyright (C) by Rami Ojala

    You are not allowed to change or share
    this file.
*/

#include "ChatChannel.h"

void ChatChannels::SendToChannel(int SourceSocket, std::string channelName, std::string message)
{
    /*

    fd_set sendToChannel = channelMap[channelName].Members;
    int socketCount = select(0, &sendToChannel, nullptr, nullptr, nullptr);

    for (int i = 0; i < socketCount; i++)
    {
        int currentSocket = sendToChannel.fd_array[i];

        if (currentSocket != SourceSocket)
        {
            send(currentSocket, message.c_str(), 1024, 0); // SEND ALL PROTOKOLLASTA TAHAN
        }
    }

    */
}

void ChatChannels::JoinChannel(std::string ChannelName)
{
    // try to find channel
    // if not found create one and join

    ServerUtilites::ThrowError("Not yet implemented.");
}

void ChatChannels::LeaveChannel()
{
    ServerUtilites::ThrowError("Not yet implemented.");
}

void ChatChannels::SetLocalChannel()
{
    // Leave old local channel
    // Join new local channel
}

void ChatChannels::LogToDatabase()
{
    ServerUtilites::ThrowError("Not yet implemented.");
}

std::pair<std::string, ChatChannel> ChatChannels::CreateChannel(std::string name)
{
    ChatChannel newChannel;
    newChannel.Name = name;
    FD_ZERO(&newChannel.Members);
    return std::make_pair(name, newChannel);
}

ChatChannels::ChatChannels()
{
    channelMap.insert(CreateChannel("Global Chat"));
    channelMap.insert(CreateChannel("Trade Chat"));
}
