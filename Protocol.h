/*#
    Copyright (C) by Rami Ojala

    You are not allowed to change or share
    this file.
*/

#ifndef PROTOCOL_H
#define PROTOCOL_H

#ifdef __unix__
    #define SOCKET_ERROR -1 // linux doesn't have this                          ///< error in socket always returns -1, this makes it more readable
    #include <unistd.h> // close socket
    #include <sys/socket.h>
    #include <string.h>
    #include <netdb.h> // addrinfo structure
#elif defined _WIN32
    #include <WS2tcpip.h>
    #pragma comment (lib, "ws2_32.lib")
#endif

#include <iostream>
#include <string>
#include "Utilities.h"

#include <vector>

#define HEADERBYTES 4                                                           ///< Number of bytes in header
#define BITSINBYTE 8                                                            ///< Bits in one byte. Used while calculating bitshift in writeHeader()

/**
    Variable Packet Protocol (VPP)

    Makes sure that all data is sent and received. Calcuates sizes automatically.

*/
class VPProtocol
{
public:
    static int ReadAll(int Socket, std::string &OutputData);                        ///< Read variable sized packet
    static int SendAll(int Socket, std::string &InputData);                         ///< Send variable sized packet
private:
    static unsigned int ReadHeader(int Socket);
    static void WriteHeader(int numberToEncode, char outputArray[]);
    static int ReadAll(int Socket, std::string &OutputData, int DataLength);
    static int SendAll(int Socket, const char *InputData, int DataLength);
};

#endif
