/*
    Copyright (C) by Rami Ojala

    You are not allowed to change or share
    this file.
*/

#ifndef SERVER_H
#define SERVER_H

#include <sys/epoll.h>
#include "Protocol.h"
#include <map>

#define MAXEVENTS 10000                          ///< this needs to be calculated from average event count per person.
#define MESSAGEBUFFERSIZE 4096

struct Connection
{

    char ClientID[10];
    char PassPhrase[64];
    char ReceiveBuffer[MESSAGEBUFFERSIZE];
    char SendBuffer[MESSAGEBUFFERSIZE];
};

// forward declatarion
class Server;

// callback to message handling
typedef void (*IncomingMessage)(Server *server, int Socket);
typedef int (*IncomingConnection)(Server *server, int Socket);

/**

    Server class
*/
class Server
{
public:
    Server(
        std::string &IPAddress,
        std::string &Port,
        IncomingMessage MessageHandler,
        IncomingConnection ConnectionHandler);
    ~Server();
    std::map<int, Connection*> GetConnections(); ///< does not return reference because we don't want implementation to mess with the connection structs
    // std::map<int, epoll_event> GetClients();

    void ShutdownServer();
    void HandleEpollEvents();

private:

    struct addrinfo Hints;
    addrinfo *AddressInformation;

    int ServerListenerSocket;

    int EpollEventListener;
    epoll_event ListenerEvents;
    epoll_event IncomingEvents[MAXEVENTS];
    int AvailableEventCount = 0;
    void AcceptIncomingConnection(epoll_event &SocketEvents);
    void CloseSingleConnection(epoll_event &SocketEvents);

    IncomingMessage HandleMessage; ///< callback function for recv and broadcast here
    IncomingConnection HandleConnection; ///< callback function for Handshakes and stuff here

    std::map<int, epoll_event> AllClients;
    std::map<int, Connection*> ConnectionInfos;

    void Initialize();
    void CreateAddress(std::string &IPAddress, std::string &Port);
    void CreateSocket();
    void StartListening();
    void CloseAllConnections();
};

#endif
