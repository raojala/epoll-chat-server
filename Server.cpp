/*
    Copyright (C) by Rami Ojala

    You are not allowed to change or share
    this file.
*/

#include "Server.h"

Server::Server(
    std::string &IPAddress,
    std::string &Port,
    IncomingMessage MessageHandler,
    IncomingConnection ConnectionHandler)
{
    HandleMessage = MessageHandler;
    HandleConnection = ConnectionHandler;
    Initialize();
    CreateAddress(IPAddress, Port);
    CreateSocket();
    StartListening();
}

Server::~Server()
{

    delete AddressInformation;
}

void Server::Initialize()
{
    std::cout << "Starting server...\n";
    memset(&Hints, 0, sizeof Hints);
}

void Server::CreateAddress(std::string &IPAddress, std::string &Port)
{

    Hints.ai_family = AF_INET;
    Hints.ai_socktype = SOCK_STREAM;
    getaddrinfo(IPAddress.c_str(), Port.c_str(), &Hints, &AddressInformation);
}

void Server::CreateSocket()
{

    ServerListenerSocket = socket(
                               AddressInformation->ai_family,
                               AddressInformation->ai_socktype,
                               AddressInformation->ai_protocol);

    EpollEventListener = epoll_create1(0);

    // read+write EPOLLIN + EPOLLOUT
    // read EPOLLIN
    // write EPOLLOUT
    // client closed connection EPOLLRDHUP
    ListenerEvents.events = EPOLLIN; // events is bitmask
    ListenerEvents.data.fd = ServerListenerSocket; // fd = socket
}

void Server::StartListening()
{

    bind(ServerListenerSocket, AddressInformation->ai_addr, AddressInformation->ai_addrlen);
    listen(ServerListenerSocket, SOMAXCONN);


    // Register ServerSocket to EpollEventListener
    // epoll_ctl(destination epoll set, command, socket, events); returns 0 or -1
    // EPOLL_CTL_ADD register socket to set
    // EPOLL_CTL_MOD edit events of socket
    // EPOLL_CTL_DEL remove socket from set
    epoll_ctl(EpollEventListener, EPOLL_CTL_ADD, ServerListenerSocket, &ListenerEvents);
    std::cout << "\tNow accepting connections...\n";
}

void Server::HandleEpollEvents()
{
    // int epoll_wait(int epfd, struct epoll_event *events, int maxevents, int timeout);
    // last parameter of epoll_wait:
    // -1 blocks until atleast one event is in the pipe
    // 0 is non blocking and continues even without any events
    // anything over 0 is milliseconds
    AvailableEventCount = epoll_wait(EpollEventListener, IncomingEvents, MAXEVENTS, -1);
    for (int i = 0; i < AvailableEventCount; i++)
    {

        // accept if incomingEvents[i].data.fd is equal to ServerListenerSocket
        // check if current socket in loop is listener
        if(IncomingEvents[i].data.fd == ServerListenerSocket)
        {
            AcceptIncomingConnection(IncomingEvents[i]); // callback function
        }
        // check if incomingEvents[i].events has EPOLLRDHUP bits pulled up and close connections
        else if (IncomingEvents[i].events & EPOLLRDHUP) // check if EPOLLRDHUP Bit is flipped
        {
            CloseSingleConnection(IncomingEvents[i]);
        }
        // check if incomingEvents[i].events has EPOLLIN bits pulled up and handle incoming message
        else if(IncomingEvents[i].events & EPOLLIN) // check if EPOLLIN bit is flipped
        {
            HandleMessage(this, IncomingEvents[i].data.fd); // callback function
        }

        // clean out incoming events
        memset(IncomingEvents, 0, sizeof(IncomingEvents));
    }
}

void Server::AcceptIncomingConnection(epoll_event &SocketEvents)
{

    int newClient = HandleConnection(this, SocketEvents.data.fd);

    Connection * newConnection = new Connection();
    memset(newConnection->SendBuffer, 0, MESSAGEBUFFERSIZE);
    memset(newConnection->ReceiveBuffer, 0, MESSAGEBUFFERSIZE);

    ConnectionInfos.insert(std::pair<int, Connection*>(newClient, newConnection));

    // create new events for new client
    epoll_event newClientEvents;
    newClientEvents.events = EPOLLIN + EPOLLRDHUP; // read and peer closed socket events
    newClientEvents.data.fd = newClient; // set event socket to new client
    epoll_ctl(EpollEventListener, EPOLL_CTL_ADD, newClient, &newClientEvents); // add new connection to the set

    AllClients.insert(std::pair<int, epoll_event>(newClient, newClientEvents));

    std::cout << '\t' << newClient << " Joined\n";
}

void Server::CloseSingleConnection(epoll_event &SocketEvents)
{

    if(close(SocketEvents.data.fd) == 0)
    {
        delete ConnectionInfos[SocketEvents.data.fd];
        ConnectionInfos.erase(SocketEvents.data.fd);
        // delete listener from epoll set
        epoll_ctl(EpollEventListener, EPOLL_CTL_DEL, SocketEvents.data.fd, &SocketEvents);
        AllClients.erase(SocketEvents.data.fd);
        std::cout << '\t' << SocketEvents.data.fd << " - client closed connection\n";
    }
    else
        std::cout << '\t' << "Could not close: " << SocketEvents.data.fd << " - connection\n";
}

void Server::CloseAllConnections()
{
    // Close sockets and remove events from epoll
    for (auto&& [Socket,SocketEvents] : AllClients)
    {
        CloseSingleConnection(SocketEvents);
    }
    std::cout<< "end close all connections" << '\n';
}

std::map<int, Connection*> Server::GetConnections()
{

    return ConnectionInfos;
}

void Server::ShutdownServer()
{
    close(ServerListenerSocket);
    CloseAllConnections();
    epoll_ctl(EpollEventListener, EPOLL_CTL_DEL, ServerListenerSocket, &ListenerEvents);
    close(EpollEventListener);
    std::cout<< "end shutdownserver" << '\n';
}
