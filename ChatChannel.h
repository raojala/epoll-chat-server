/*
    Copyright (C) by Rami Ojala

    You are not allowed to change or share
    this file.
*/

#ifndef CHATCHANNEL_H
#define CHATCHANNEL_H

#include "Protocol.h"
#include <string>
#include <map>

/**
    Channel

    Stores channel name and member sockets
    for sending messages.
*/
struct ChatChannel
{
    std::string Name;                                                   ///< Name of the channel
    fd_set Members;                                                     ///< Sockets for all the members that have joined this channel
};

/**
    Channels


*/
class ChatChannels
{
public:
    ChatChannels();
    void SendToChannel(int SourceSocket, std::string channelName, std::string message); ///< Sends message to everyone but the sender in a given Channel
    void JoinChannel(std::string ChannelName);                          ///< Places senders socket to given channels fd_set so messages can be received
    void LeaveChannel();                                                ///< Remove senders socket from channels fd_set to not receive anymore messages
    void SetLocalChannel();                                             ///< Set senders socket to new local channel and remove from old.
private:
    void LogToDatabase();
    std::map<std::string, ChatChannel> channelMap;
    std::pair<std::string, ChatChannel> CreateChannel(std::string name);
};

#endif
